/* eslint-disable no-undef */

import cluster from 'cluster';
import os from 'os';

if (cluster.isMaster) {
  const cpus = os.cpus().length;
  for (let i = 0; i < cpus; ++i) {
    cluster.fork();
  }
} else {
  require('./service.js');
}
