/* eslint-disable no-console */

import http from 'http';
import { URL } from 'url';
import process from 'process';
import console from 'console';
import chalk from 'chalk';
import processQuery from './lib/utils';
import { logInfo, logError } from './lib/logger';

const PORT = 8080;
logInfo(`${chalk.yellow('Launching:')} http server.`, false);

process.on('SIGINT', () => {
  console.log(
    `${chalk.gray(new Date().toString())} [${process.pid}] ${chalk.redBright(
      'Terminating',
    )} process.`,
  );
  process.exit(0);
});

http
  .createServer(async (req, res) => {
    const absUrl = 'http://' + req.headers['host'] + req.url;
    const url = new URL(absUrl);
    if (url.pathname.toLowerCase() === '/test' && url.search) {
      try {
        const result = await processQuery(url.searchParams);
        res.statusCode = 200;
        res.end(result);
      } catch (error) {
        logError(error);
        res.statusCode = 500;
        res.end(error.message);
      }
    } else {
      res.statusCode = 404;
      res.end('Url invalid.');
    }
  })
  .listen(PORT, () => {
    logInfo(`${chalk.green('Running:')} Service listening on port ${PORT}`),
      false;
  });
