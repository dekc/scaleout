import process from 'process';
import console from 'console';
import chalk from 'chalk';

const dateTimeStamp = (format = 'short') => {
  const ts = new Date();
  if (format === 'short') {
    const dateStr = `${(ts.getMonth() + 1).toString().padStart(2, '0')}/${ts
      .getDate()
      .toString()
      .padStart(2, '0')}/${ts.getFullYear()}`;
    const timeStr = ts.toTimeString(); //`${ts.getHours()}:${ts.getMinutes()}:${ts.getSeconds()}`;
    return `${dateStr} ${timeStr}`;
  }
};

const logInfo = (info, prefix = true) => {
  const message = `${chalk.gray(dateTimeStamp())} [${process.pid}] ${
    prefix ? chalk.whiteBright('Info:') : ''
  } ${info}`;
  console.info(message);
};

const logError = error => {
  let message = '';
  if (error instanceof String) {
    message = `${chalk.gray(dateTimeStamp())} [${process.pid}] ${chalk.red(
      'Error:',
    )} ${error}`;
  }
  if (error instanceof Error) {
    message = `${chalk.gray(dateTimeStamp())} [${process.pid}] ${chalk.red(
      'Error:',
    )} ${error.message}`;
  }
  console.error(message); // esline-disable-line no-console
  return new Error(message);
};

export { logInfo, logError };
