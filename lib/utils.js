import process from 'process';
import { setTimeout } from 'timers';
import { logInfo } from './logger';

const pid = process.pid;

const timeoutAsync = (duration, cb) => {
  return new Promise(resolve => {
    setTimeout(() => resolve(cb()), duration * 1000);
  });
};

const workResultForAsyncCall = () =>
  `Service async process [${pid}]: completed `;

const simulateWorkByCpu = repeat => {
  let result = 0;
  const loop = repeat || 1e8;
  const start = process.hrtime();
  logInfo('Handling CPU bound request.');
  for (let i = 0; i <= loop; ++i) {
    result += Math.PI / Math.pow(Math.E, 2);
  }
  const diff = process.hrtime(start);
  const ms = (diff[0] + diff[1] / 1e9).toFixed(3);
  return `CPU bound process [${pid}]: ${result} (${ms}s)`;
};

const simulateWorkByDelay = async (duration, force = false) => {
  if (duration > 300 && !force) {
    return Promise.reject(
      new Error('Delay exceeds 5 mins. Use forceDelay in query params'),
    );
  }
  logInfo(`Handling ${duration} second delay request.`);
  const start = process.hrtime();
  const result = await timeoutAsync(duration, workResultForAsyncCall);
  const delta = process.hrtime(start);
  const ms = (delta[0] + delta[1] / 1e9).toFixed(3);

  return `${result} (${ms}s)`;
};

const processQuery = queryParams => {
  const name = queryParams.get('cmd').toLowerCase();
  let value = '';
  if (!name) {
    throw new Error('"cmd" query parameter missing.');
  }
  switch (name) {
    case 'wait':
      value = Number.parseInt(queryParams.get('delay'));
      return simulateWorkByDelay(value, queryParams.has('forceDelay'));

    case 'cpu':
      value = queryParams.get('loop');
      return simulateWorkByCpu(value);

    default:
      return Promise.reject(Error(`command "${name}" not recognized`));
  }
};

export default processQuery;
